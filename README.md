# Online NVivo Labs by Research Computing Services

Hey Everyone!

This repository contains basic training materials to familiarise you with NVivo 12 - Computer Assisted Qualitative Data Analysis Software (CAQDAS) developed by QSR International. 

This resource is designed for researchers at the University of Melbourne. Staff and students at the University can download and access a full license for NVivo at: http://go.unimelb.edu.au/bt9r

This is a self-paced learning lab. There are three modules which you can work through to learn NVivo:

## Module 1: ResPitch

The NVivo ResPitch contains a very short introductory challenge to give you a quick taste of the software. Essentially, I'll be pitching to you (hence the name) why you might want to consider NVivo as a digital tool for your research. You will learn how to import and code files in NVivo, and why it's an efficient tool for qualitative and mixed-methods research. Get started with this module here: http://go.unimelb.edu.au/2d9r

## Module 2: Introduction to NVivo

The Introduction to NVivo module will provide a broader introduction to and understanding of NVivo. Whilst you will again import and code files, this will be framed within qualitative research methodologies. NVivo is just one tool of many that you can use in research, and this Introduction will highlight the strengths and limitations of the tool. You will also learn the importance of questions in relation to your research question, and how to do a basic Word Cloud visualisation. 

## Module 3: Intermediate NVivo Skills

This final module will teach you some more advanced functions of NVivo. In this lab, you will again learn how to code cases and create classifications. You will also learn how to code for sentiment. You will learn how to create a range of interesting and meaningful visualisations. Finally, you will learn how to keep track of your research through memos and annotations.

# Further Resources

Files prefixed with FR are further open-source resources which you can use to develop your understanding of NVivo beyond the scope of these learning labs.

# Any Questions?

If you have any questions, you can get in touch with me at alex.shermon@unimelb.edu.au

You can follow me on twitter @alexshermon. 

To stay up to date with the NVivo community, make sure to sign up to the monthly email list: http://eepurl.com/gExj6f
